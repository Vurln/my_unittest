FROM python:3.10
COPY . .
RUN pip install flask python
CMD ["python", hello.py"]
